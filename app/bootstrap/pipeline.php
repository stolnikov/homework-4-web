<?php

use FastRoute\Dispatcher;
use Psr\Container\ContainerInterface;

/**
 * Setup middleware queue
 *
 * @param Dispatcher $routes
 * @param ContainerInterface $container
 *
 * @return array
 */
return function (Dispatcher $routes, ContainerInterface $container): array {

    $middlewareQueue[] = new Middlewares\FastRoute($routes);
    $middlewareQueue[] = new Middlewares\RequestHandler($container);

    return $middlewareQueue;
};

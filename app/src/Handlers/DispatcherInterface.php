<?php

namespace BracyWeb\Handlers;

/**
 * Bracy dispatcher participates in processing user request.
 */
interface DispatcherInterface
{
    /**
     * Process an incoming client request and return a text response delegating
     * response finalizing to output handler.
     *
     * @param  string $buffer the information received over the socket
     *
     * @return mixed
     */
    public function process(string $buffer);
}

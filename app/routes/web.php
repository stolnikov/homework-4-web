<?php

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

return simpleDispatcher(
    function (RouteCollector $routes) {
        $routes->post('/', BracyWeb\Http\ActionControllers\IndexPostHandler::class);
    }
);

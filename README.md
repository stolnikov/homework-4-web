# Bracy docker web server

PSR compatible HTTP server leveraging [stolnikov/bracy package](https://github.com/stolnikov/bracy).

![BracyWeb](https://drive.google.com/uc?export=view&id=1rxE5rvSzWWC51wgDs-Rs7nq7P4JbqQF6)

Self-called anonymous function in index.php acts as entry point. It creates its own scope and keeps the global namespace clean.

# PSR compatibility

**PSR-11 DI container**: [PHP-DI](https://github.com/PHP-DI/PHP-DI).

**PSR-15 middleware dispatcher**: [Relay](https://github.com/relayphp/Relay.Relay) request handler.

**PSR-7 messages**: PSR-15 middleware spec requires implementations to pass along PSR-7 compatible HTTP messages. [Zend Diactoros](https://github.com/zendframework/zend-diactoros/) has PSR-7 implementation of PSR-7 interfaces as well as emitters for flushing PSR-7 responses.

# Project structure
```
├── app
│   ├── bootstrap
│   │   ├── container.php
│   │   └── pipeline.php
│   ├── composer.json
│   ├── public
│   │   └── index.php
│   ├── routes
│   │   └── web.php
│   └── src
│       ├── Handlers
│       │   ├── BracyStatusDispatcher.php
│       │   └── DispatcherInterface.php
│       └── Http
│           └── ActionControllers
│               └── IndexPostHandler.php
├── docker
│   └── nginx
│       ├── conf.d
│       │   └── nginx.conf
│       └── Dockerfile
├── docker-compose.yml
└── README.md
```
# Usage

**Example 1.**
```
POST / HTTP/1.1
Host: type-any-host
Content-Type: application/x-www-form-urlencoded
Content-Length: 25

string=((((()))()(())()))
```
Output:
```
HTTP/1.1 200 OK
Server: nginx/1.14.0 (Ubuntu)
Content-Type: text/plain; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive

27
Check complete. Brackets are balanced.

0
```
**Example 2.**
```
POST / HTTP/1.1
Host: type-any-host
Content-Type: application/x-www-form-urlencoded
Content-Length: 14

string=((*))[]
```
Output:
```
HTTP/1.1 400 Bad Request
Server: nginx/1.14.0 (Ubuntu)
Content-Type: text/plain; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive

2d
Provided string contains invalid characters.

0
```
**Example 3.** Empty string.
```
POST / HTTP/1.1
Host: type-any-host
Content-Type: application/x-www-form-urlencoded
Content-Length: 7

string=
```
Output:
```
HTTP/1.1 400 Bad Request
Server: nginx/1.14.0 (Ubuntu)
Content-Type: text/plain; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive

1a
Provided string is empty.

0
```
**Example 4.** No 'string' parameter in the request.
```
POST / HTTP/1.1
Host: type-any-host
Content-Type: application/x-www-form-urlencoded
Content-Length: 0
```
```
HTTP/1.1 400 Bad Request
Server: nginx/1.14.0 (Ubuntu)
Content-Type: text/plain; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive

27
Parameter 'string' has not been passed.
0
```